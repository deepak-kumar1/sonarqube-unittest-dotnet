#!/bin/sh
timestamp=$(date +"%Y%m%d%H%M%S")
version="v$timestamp"
echo "$version" > version.txt
